const { container: Container, codecept: Codecept } = require("codeceptjs");
const databaseOperations = require("./Framework/FrameworkUtilities/DatabaseOperations/databaseOperations");
const LMT=require("./Framework/FrameworkUtilities/i18nutil/readI18NProp");
const prop = require('./PropertiesConfigurator');
global.confi_prop = prop;
const LOGIN_URL = 'https://login-qcvw.zycus.net/sso/';
const DD_URL = "https://dewdrops-qcvw.zycus.net/isource/#/events";
const BROWSER = 'chrome';
global.lang = 'en';
async function runCodecept() {

    const config = {
        tests: './iSource/Implementation/**/**.js',
        output: './output',
        helpers: {
            WebDriver: {
                url:prop.url,
                browser: prop.browser,
                host:prop.host,
                port: prop.port,
                restart:false,
                windowSize: prop.windowSize,
                smartwait:prop.DEFAULT_MEDIUM_WAIT,
                waitForTimeout: 30000,
                default_low_wait: prop.DEFAULT_LOW_WAIT,
                default_medium_wait: prop.DEFAULT_MEDIUM_WAIT,
                default_high_wait: prop.DEFAULT_HIGH_WAIT,
              },
            "ChaiWrapper":
            {
                "require": "codeceptjs-chai"
            },
             MyHelper: {
                  require: './Framework/CustomHelper/myHelper.js',
                }, 
        },

        
        gherkin: {
            features: './iSource/features/**/**.feature',
            steps: './iSource/Implementation/**/**.js'
        },

        name: 'iSource_UI_Automation',
        plugins: {
            retryFailedStep: {
                enabled:false
            },
            screenshotOnFail: {
                enabled: true
            },
            wdio: {
                enabled: true,
                services: ['selenium-standalone']
            },autoDelay: {
                enabled: true,
                delayBefore: 1000,
                delayAfter: 500,
                methods: ['click', 'fillField', 'checkOption']
            }
        }
    }

    const opts = {
         steps: true,
        //grep: "@tag4",
        verbose: true
    }

    const codecept = new Codecept(config, opts);

    // initialize codeceptjs in current dir
    codecept.initGlobals(__dirname);

    // create helpers, support files, mocha
    Container.create(config, opts);

    // initialize listenept.runHooks();ers
    codecept.runHooks();

    //logger.info("before bootstrap");

     global.testData = await databaseOperations.getTestData();
    
    global.uiElements = await databaseOperations.getUiElementXpath();
    global.commonuiElements = await databaseOperations.getCommonUiElementXpath();
   global.allkeys = await databaseOperations.getLMTKeys();
  global.lmt = await databaseOperations.getLMTDetails(); 
  //console.log("**************"+LMT.getLabelFromKey("Supplier Name"));
    
    // codecept.runBootstrap(async (err) => {
        // load tests
        // codecept.loadTests("*_test.js");
        //codecept.loadTests("./iSource/features/**/**/ViewAllSuppliersFeatures.feature");
        codecept.loadTests("./iSource/features/**/**/Myevents.feature");

       // logger.info("****************************** before codecept run**********************************")

        // run tests
        codecept.run();
    // });
}

runCodecept();

module.exports = { runCodecept };
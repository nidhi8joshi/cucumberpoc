import { Startup } from "../FrameworkUtilities/Startup/Startup";

// const {error} = require('codeceptjs').output.error;
// const {recorder} = require('codeceptjs').recorder;
import {event} from 'codeceptjs';
// import {Jiralogger} from "../Jiralogging/Jiralogging"
import { prop } from "../FrameworkUtilities/config";
import { Jiralogger } from "../Jiralogging/Jiralogging";
let currentTest : string;
let currentHook : string;
let errorOccured = false;
let JiraloggingFlag = prop.JiraloggingFlag || process.env.JiraloggingFlag;

export class eventListener{
static async init() {

  event.dispatcher.on(event.test.started, async function () {
    console.log('--- I am before test --');

  })

  event.dispatcher.on(event.test.after, async function () {
    console.log('--- I am after test --');

  })

  event.dispatcher.on(event.test.failed, async function (test, e) {
    errorOccured = true;
    console.log('--- I am failed test --' + errorOccured);
  })

  event.dispatcher.on(event.test.finished, async function (test, err) {
    // currentTest = null
    console.log('--- I am finished test --' + errorOccured +"  ==  "+ JiraloggingFlag);
    // console.log("==================================================")
    if(errorOccured && JiraloggingFlag){
      // console.log("Cucumber step that failed "+test.title);
      // console.log("Cucumber feature name along wth the tag"+test.parent.title)
      // console.log("Error  "+test.err);
      // console.log("previous Step "+test.err.stack);
      // console.log("Error occured and logging the jira bug")
      await Jiralogger.logJira(test.title, test.parent.title, test.err.stack);
    }
  })

  event.dispatcher.on(event.suite.after, (suite) => {
    console.log("----- After suite-------")
  });

  event.dispatcher.on(event.test.before, async function (test, err) {
        global.scenarioName = test.title;
        global.moduleName = test.parent.title;
      });
    
  event.dispatcher.on(event.step.before, async function (step, err) {
        global.Step=step;
      });
}
}
import { Startup } from "../Startup/Startup";
import { DatabaseOperations } from "../DatabaseOperations/databaseOperations";
import { parseJson } from "../../PropertiesConfigurator/PropertiesConfigurator";


export class iSourceBootstrap{
    static async boot_isource(){
        await parseJson();
        Startup.users = await DatabaseOperations.getUser();
        Startup.testData = await DatabaseOperations.getTestData();
        Startup.uiElements = await DatabaseOperations.getUiElementXpath();
        Startup.users = await DatabaseOperations.getUser();
        Startup.lmt = await DatabaseOperations.getLMTDetails();
        Startup.allkeys = await DatabaseOperations.getLMTKeys();
        Startup.automationLMT_Pair = await DatabaseOperations.getAutomationKey();
        Startup.commonuiElements=await DatabaseOperations.getCommonUiElementXpath();
        Startup.lang = "en";
    }
}
import { DatabaseOperations } from "../DatabaseOperations/databaseOperations.js";
import { Startup } from "../Startup/Startup.js";
import { logger } from "../Logger/logger.js";
import { prop } from "../config.js";
// const sharedData = require("../../../../Share_data");
import { eventListener } from "../../CustomHelper/eventListeners";
import {parseJson } from "../../PropertiesConfigurator/PropertiesConfigurator"
export async function bootstrap() {
        parseJson();
        eventListener.init();
        Startup.lang = 'en';
        Startup.testData = await DatabaseOperations.getTestData();
        Startup.uiElements = await DatabaseOperations.getUiElementXpath();
        Startup.users = await DatabaseOperations.getUser();
        Startup.lmt = await DatabaseOperations.getLMTDetails();
        Startup.allkeys = await DatabaseOperations.getLMTKeys();
        Startup.commonuiElements = await DatabaseOperations.getCommonUiElementXpath();
        Startup.automationLMT_Pair = await DatabaseOperations.getAutomationKey();

        logger.info("for this Chunk USERNAME  : " + Startup.users.get("USERNAME"));
}

export async function teardown() {
        // sharedData.stopShareDataService();
        // if(prop.runOnGrid) {
                // await DatabaseOperations.updateUSER(Startup.users.get("USERNAME") as string, "true");
        // }
}

export async function bootstrapAll() {
        logger.info("inside bootstrapAll");
}

export async function teardownAll() {
        logger.info("inside tearDownAll");
}
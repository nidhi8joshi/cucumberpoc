import * as jwt from 'atlassian-jwt';
import moment from 'moment';

const now = moment().utc();

// Simple form of [request](https://npmjs.com/package/request) object
const req: jwt.Request = jwt.fromMethodAndUrl('POST', 'https://xray.cloud.xpand-it.com');

const tokenData = {
    "iss": 'nidhi.joshi',
    "iat": now.unix(),                    // The time the token is generated
    "exp": now.add(3, 'minutes').unix(),  // Token expiry time (recommend 3 minutes after issuing)
    "qsh": jwt.createQueryStringHash(req) // [Query String Hash](https://developer.atlassian.com/cloud/jira/platform/understanding-jwt/#a-name-qsh-a-creating-a-query-string-hash)
};

const secret = 'b45f7efeb7639fcc1a38c88e7040e22fcfae4c14f9a4b02123a500803dc7aaac';

const token = jwt.encode(tokenData, secret);
console.log(token);
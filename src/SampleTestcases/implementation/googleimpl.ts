import "codeceptjs";
declare const inject: any;
const { I } = inject();


Given('user is on google page', async function () {
    I.amOnPage("https://www.google.com/")
});


Given('user searcheds for the cucumber keyword', async function () {
    I.fillField("//input[@name='q']", "cucumber bdd")
});


Given('user should see the results', async function () {
    I.see("cucumber.io")
});
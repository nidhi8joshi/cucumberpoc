@REQ_DDS-36433
Feature: Creation of charts
	## Sheet System
	## Chart Types
	## Charting Sheet Layout
	## Left Panel
	## Export

	@TEST_DDS-187086 @REQ_DDS-36435 @DDS-36435 @tagL3
	Scenario: User should be able to navigate between main sheet and chart sheets seamlessly
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create a report with dimension and measure and save it
		Then I select the currency for the converison
		Then I click on Add chart button in the footer section
		Then I verify user is taken on chart sheet
		Then I click on Table sheet in the footer section
		Then I verify user is taken to Table sheet
	@TEST_DDS-187087 @REQ_DDS-36435 @DDS-36435 @tagL4
	Scenario: User should be able to rename a sheet from sheet ellepsis
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create a report with dimension and measure and save it
		Then I select the currency for the converison
		Then I click on Add chart button in the footer section
		Then I click on menu option in the footer section of the sheet
		Then I select rename option
		Then I enter the name and save it
	@TEST_DDS-187088 @REQ_DDS-36435 @DDS-36435 @tagL4
	Scenario: User should be able to delete a sheet from sheet ellepsis
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create a report with dimension and measure and save it
		Then I select the currency for the converison
		Then I click on Add chart button in the footer section
		Then I click on menu option in the footer section of the sheet
		Then I select delete option
		Then I click on yes to delete the sheet
		Then I verify sheet is deleted
	@TEST_DDS-187089 @REQ_DDS-36435 @DDS-36435 @tagL4
	Scenario: User should be able to duplicate a sheet from sheet ellepsis
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create a report with dimension and measure and save it
		Then I select the currency for the converison
		Then I click on Add chart button in the footer section
		Then I click on menu option in the footer section of the sheet
		Then I select duplicate option
		Then I verify duplicate sheet is seen
	@TEST_DDS-187090 @REQ_DDS-36435 @DDS-36435 @tagL3
	Scenario: Given one dimension and measure user should be able to plot a default chart
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create a report with dimension and measure and save it
		Then I select the currency for the converison
		Then I click on Add chart button in the footer section
		Then I verify user is taken on chart sheet
		Then I verify Default chart for one D and One M is plotted
	@TEST_DDS-187091 @REQ_DDS-36435 @DDS-36435 @tagL3
	Scenario: User should be able to export report with multiple chart sheets
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create a report with dimension and measure and save it
		Then I select the currency for the converison
		Then I click on Add chart button in the footer section
		Then I click on Table sheet in the footer section
		When I click on Export Icon
		When I enter report name
		When I select XLS File Type in the popup
		When I click on Export button
		When I click on Dismiss button

@REQ_TSTNG-22
Feature: Clause creation in iContract
	#User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add default clause
	#User should be able to add alternative clause
	#User should be able to add fallback clause
	#
	#User should be able to add various category and languages along with name number and type

	Background:
		#@PRECOND_TSTNG-42
		Given logged in to iContract Login Page
		#@PRECOND_TSTNG-57
		Given logged in to iContract Login Page

	@TEST_TSTNG-40 @tagL2
	Scenario: To verify Bulk Action activity under Authoring
		Given logged in to iContract Login Page
		Given I navigate to Authoring Contract Tab
		And I Initiate Authoring Contract with Template form Template Library
		Then I enter Contract Details
		And I add LineItem details under authoring or Repository tab
		And I click on save as draft
		And I navigate via breadcrum menu to Authoring Contract Tab
		When I search and select the contracts ony by one under Authoring
		Then I perform the Download Line Items bulk activity
		And I search and select the contracts ony by one under Authoring
		Then I perform the Download Documents bulk activity
		And I search and select the contracts ony by one under Authoring
		Then I perform the Delete bulk activity
	@TEST_TSTNG-41 @tagL2
	Scenario: To verify Bulk Action activity under Clauses
		Given logged in to iContract Login Page
		Given Click on Clause library tab
		And I click "createClause" clause BUtton
		Then Fill in all the mandatory fields on the page
		And Search for reviewer in field to assign reviewer
		And I add default clause and make clause active
		When I search and select the clause
		Then I perform the Deactivate clauses bulk activity
		And I search and select the clause
		Then I perform the Activate clauses bulk activity
		And I search and select the clause
		Then I perform the Download clauses bulk activity

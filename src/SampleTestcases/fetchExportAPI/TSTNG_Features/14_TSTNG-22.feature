@REQ_TSTNG-22
Feature: Clause creation in iContract
	#User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add default clause
	#User should be able to add alternative clause
	#User should be able to add fallback clause
	#
	#User should be able to add various category and languages along with name number and type

	#Tests User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add default clause
	#User should be able to add alternative clause
	#User should be able to add fallback clause
	@TEST_TSTNG-24 @TESTSET_TSTNG-28
	Scenario: Fill in all details and add different clauses
		Given User has created a clause
		And User is on clause details page in edit mode
		When User add default clause
		And User add alternate clause
		And User add fallback clause
		And User clicks on save button
		Then User lands on the clauses listing screen
		And Clause is saved

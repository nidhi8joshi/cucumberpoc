@REQ_TSTNG-25
Feature: Create template from the clause library
	#User should be able to create a template from icontract -> Templates screen
	#
	#User should be able to add clauses in the template outline

	Background:
		#@PRECOND_TSTNG-45
		Given logged in to iContract Login Page

	@TEST_TSTNG-46 @TESTSET_TSTNG-28 @tagL2
	Scenario: To verify that suggested action are displayed correctly for active status
		Given logged in to iContract Login Page
		Given Click on Clause library tab
		Given I click "createClause" clause BUtton
		Then Fill in all the mandatory fields on the page
		Then Search for reviewer in field to assign reviewer
		And I add default clause and make clause active
		And I verify that View is displayed as suggested acion

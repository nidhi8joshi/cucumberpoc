@REQ_TSTNG-22
Feature: Clause creation in iContract
	#User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add default clause
	#User should be able to add alternative clause
	#User should be able to add fallback clause
	#
	#User should be able to add various category and languages along with name number and type

	Background:
		#@PRECOND_TSTNG-44
		Given logged in to iContract Login Page
		#@PRECOND_TSTNG-52
		Given logged in to iContract Login Page

	@TEST_TSTNG-43 @tagL2
	Scenario: Searching clauses by Title
		Given logged in to dewdrops
		Given I Navigate to iContract Tab Clauses Tab
		Then I should be able to search Clauses by Clause Title

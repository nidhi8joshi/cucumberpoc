@REQ_TSTNG-22
Feature: Clause creation in iContract
	#User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add default clause
	#User should be able to add alternative clause
	#User should be able to add fallback clause
	#
	#User should be able to add various category and languages along with name number and type

	Background:
		#@PRECOND_TSTNG-39
		Given logged in to iContract Login Page
		#@PRECOND_TSTNG-45
		Given logged in to iContract Login Page

	@TEST_TSTNG-36 @TESTSET_TSTNG-28 @TESTSET_TSTNG-29 @tagL1
	Scenario: To create a new clause and assign clause reviewers to that particular Clause.
		Given logged in to iContract Login Page
		Given Click on Clause library tab
		Given I click "createClause" clause BUtton
		Then Fill in all the mandatory fields on the page
		Then Search for reviewer in field to assign reviewer
		Then I click Save button
	@TEST_TSTNG-37 @tagL1
	Scenario: To add Alternate/fallback clause to existing clause
		Given logged in to iContract Login Page
		Given Click on Clause library tab
		Given I click "createClause" clause BUtton
		Then Fill in all the mandatory fields on the page
		Then Click on Actions link displayed to respective to clause
		Then Click on view option
		Then add alternate clause
		Then I Enter FallBack Clause
		Then I click Save button
	@TEST_TSTNG-38 @REQ_TSTNG-25 @tagL2
	Scenario: To verify that suggested action are displayed correctly for inactive status
		Given logged in to iContract Login Page
		Given Click on Clause library tab
		Given I click "createClause" clause BUtton
		Then Fill in all the mandatory fields on the page
		Then I verify that Add Default is displayed as suggested action
		Then I add default clause
		And I verify that View is displayed as suggested acion

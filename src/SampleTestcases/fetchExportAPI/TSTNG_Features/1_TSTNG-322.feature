@REQ_TSTNG-322
Feature: Jira story for trying he tag mapping
	#Jira story for trying he tag mapping

	@TEST_TSTNG-323 @TSTNG-322 @tagL1
	Scenario: scenario one created for tag mapping
		Given loggin to application
		When tagged an story
		Then should see the story in linked issue
	@TEST_TSTNG-324 @TSTNG-322 @tagL1
	Scenario: scenario two created to check feature update is impacting the linkage
		Given loggin to application
		When new scenario added to same files
		Then scenario should be added to the story
	@TEST_TSTNG-325 @TSTNG-322 @tagL1
	Scenario: scenario three created to understand the issue with import cucumber test feature
		Given loggin to application
		When understand the issue with import cucumber test feature
		Then scenario should be added to the story
	@TEST_TSTNG-326 @TSTNG-322 @tagL1
	Scenario: scenario four created to re-iterate the same scenario as scenario three
		Given loggin to application
		When re-iterate the same scenario as scenario three
		Then scenario should be added to the story
	@TEST_TSTNG-327 @TSTNG-322 @tagL1
	Scenario: first scenario from second file
		Given loggin to application
		When first scenario from second file
		Then should see the story in linked issue
	@TEST_TSTNG-329 @TSTNG-322 @tagL1
	Scenario: second scenario from another fie to test the update features
		Given Login to application
		When second scenario from another fie to test the update features
		Then Should see on jira

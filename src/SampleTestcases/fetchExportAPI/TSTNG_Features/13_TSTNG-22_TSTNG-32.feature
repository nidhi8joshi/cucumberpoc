@REQ_TSTNG-22
Feature: Clause creation in iContract
	#User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add default clause
	#User should be able to add alternative clause
	#User should be able to add fallback clause
	#
	#User should be able to add various category and languages along with name number and type

	Background:
		#@PRECOND_TSTNG-32
		When User login to application

	#Tests User should be able to create a clause in the icontract application from "Clauses" tab
	#
	#User should be able to add various category and languages along with name number and type
	@TEST_TSTNG-23 @TESTSET_TSTNG-28 @TESTSET_TSTNG-29
	Scenario: Create a clause with all required details
		Given User is logged in to the application
		And User navigates to iContract->Clauses screen
		When User initiates the clause creation
		And User fills all details from the transition screen
		Then User should land on the clause details screen in edit mode

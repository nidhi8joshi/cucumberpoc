@REQ_TSTNG-25
Feature: Create template from the clause library
	#User should be able to create a template from icontract -> Templates screen
	#
	#User should be able to add clauses in the template outline

	@TEST_TSTNG-49 @TSTNG-29
	Scenario: Testing the feature uploaded with JSON
		Given User is logged in to the application
		And User navigates to create template screen
		Then User should see a Create Template button
	@TEST_TSTNG-50 @REQ_TSTNG-29
	Scenario: Testing the feature uploaded with JSON for the second time
		Given User is logged in to the application
		And User navigates to create template screen
		Then User should see a Create Template button
	@TEST_TSTNG-51 @REQ_TSTNG-29 @TSTNG-31
	Scenario: Testing the feature uploaded with test execution tag
		Given User is logged in to the application
		And User navigates to create template screen
		Then User should see a Create Template button
	@TEST_TSTNG-53 @tagL2
	Scenario: To verify whether the contract is opening as per the status Draft In Progress
		Given logged in to iContract Login Page
		Given I navigate to Authoring Contract Tab
		And I Initiate Authoring Contract with Template form Template Library
		Then I enter Contract Details
		When I select Contracting Party And Contract Person
		Given I navigate to listing page
		Given I checked as contract owner if contract accessible
		Given I see the contract is accessible or not for "Draft in Progress"
		Then I should be able to edit "Draft in Progress"
		Then I should verify the footer buttons for "Draft in Progress"
	@TEST_TSTNG-54 @tagL2
	Scenario: To verify whether the contract is opening as per the status Pending Authoring Review
		Given logged in to iContract Login Page
		Given I navigate to Authoring Contract Tab
		When I initiate authoring contract for type subtype for which Authoring Workflow in confirgured
		Then I enter Contract Details
		When I select Contracting Party And Contract Person
		Given I navigate to listing page
		Given I checked as contract owner if contract accessible
		Given I see the contract is accessible or not for "Pending Authoring Review"
		Then I should be able to edit "Pending Authoring Review"
		Then I should verify the footer buttons for "Pending Authoring Review"
	@TEST_TSTNG-55 @tagL2
	Scenario: To verify whether the contract is opening as per the status Ready For Signing
		Given logged in to iContract Login Page
		Given I navigate to Authoring Contract Tab
		And I Initiate Authoring Contract with Template form Template Library
		Then I navigate to Details tab
		Then I enter Contract Details
		When I select Contracting Party And Contract Person
		When I save the authoring contract
		Then I Send For Contracting Party under Negotiate Stage
		Then I upload the supplier copy
		Then I initiate proceed to signing
		Given I navigate to listing page
		Given I checked as contract owner if contract accessible
		Given I see the contract is accessible or not for "Ready For Signing"
		Then I should not be able to edit "Ready For Signing"
		Then I should verify the footer buttons for "Ready For Signing"
	@TEST_TSTNG-56 @tagL2
	Scenario: To verify whether the contract is opening as per the status Pending External Review
		Given logged in to iContract Login Page
		Given I navigate to Authoring Contract Tab
		And I Initiate Authoring Contract with Template form Template Library
		Then I enter Contract Details
		When I select Contracting Party And Contract Person
		Then I send contract under Pending External Review
		Given I navigate to listing page
		Given I checked as contract owner if contract accessible
		Given I see the contract is accessible or not for "Pending External Review"
		Then I should be able to edit "Pending External Review"
		Then I should verify the footer buttons for "Pending External Review"

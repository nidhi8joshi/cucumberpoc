@REQ_DDS-183186
Feature: Mercury aVOC- File character limit upto 255
	#*Goal*- I should be able to upload files with name upto 255 characters
	#
	#*Business Scenario*- Records management document naming convention results in filename size which can exceed the limits in Zycus and Windows allows filename size up to 255 characters.
	#
	#*Acceptance Criteria*
	#
	## iRequest standard/ flexi file name size should  be upto 255 characters including extensions
	## This should be applicable for all the existing extension types
	## I should be able to upload and download extensions with file name size upto 255 chars
	#
	#
	#
	#*Gherkin steps*
	#
	#
	#
	#Scenario: User should be able to upload files with file name upto 255 characters including extensions
	#
	#Given I am logged in on iRequest
	#
	#Given I am on Create request page
	#
	#Then I upload a file with file name upto 255 characters on iRequest standard attachment field
	#
	#Then I should be able to submit the request
	#
	#----
	#
	#Scenario: User should be able to upload files with file name upto 255 characters including extensions
	#
	#Given I am logged in on iRequest
	#
	#Given I am on Create request page
	#
	#Then I upload a file with file name upto 255 characters on iRequest flexiform attachment field
	#
	#Then I should be able to submit the request
	#
	#----
	#
	#Scenario: User should be able to download files with file name upto 255 characters including extensions
	#
	#Given I am logged in on iRequest
	#
	#Given I am on View request page
	#
	#Then I  should be able download standard attachment upto 255 character
	#
	#----
	#
	#
	#
	#Scenario: User should be able to download files with file name upto 255 characters including extensions
	#
	#Given I am logged in on iRequest
	#
	#Given I am on View request page
	#
	#Then I  should be able download flexi form attachment upto 255 character
	#
	#----

	@TEST_DDS-186797 @DDS-183186
	Scenario: User should be able to upload files with file name upto 255 characters including extensions on iRequest flexiform attachment field
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for flexi field
		Then I upload a file with file name upto 255 characters on iRequest flexiform attachment field
		Then I should be able to submit the request
	@TEST_DDS-186798 @DDS-183186
	Scenario: User should be able to upload files with file name upto 255 characters including extensions on iRequest standard attachment field
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for standard field
		Then I upload a file with file name upto 255 characters on iRequest standard attachment field
		Then I should be able to submit the request
	@TEST_DDS-186799 @DDS-183186
	Scenario: User should be able to download flexiform attachment files with file name upto 255 characters including extensions
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for flexi field
		Then I upload a file with file name upto 255 characters on iRequest flexiform attachment field
		Then I should be able to submit the request
		Then I am on View request page of flexiform
		Then I should be able download flexi form attachment upto 255 character
	@TEST_DDS-186800 @DDS-183186
	Scenario: User should be able to download files with file name upto 255 characters including extensions
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for standard field
		Then I upload a file with file name upto 255 characters on iRequest standard attachment field
		Then I should be able to submit the request
		Then I am on View request page
		Then I should be able download standard attachment upto 255 character

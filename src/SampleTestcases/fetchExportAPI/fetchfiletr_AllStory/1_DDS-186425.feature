@REQ_DDS-186425
Feature: Gherkin- Mercury aVOC- Cancel Request
	#*Goal*: I should be able to cancel a request so that it does not appear as a transit request in my system.
	#
	#*Business scenario*- A Request Master or Request Manager can mark a request as cancelled if it is no longer valid. Eg- Duplicate requests, contract deleted, sourcing event cancelled, etc.
	#
	#*Customers vote*- American Regent, Auroizon, CDK, Crown, Daiichi, GPI, Guardian, Hitachi, Lowes, LKE
	#
	#*Acceptance Criteria*
	#
	#Note: Persona wise listing pages
	#
	#Requester- My Request listing
	#
	#Request Manager- My workbench
	#
	#Super request manager- all workbench
	#
	#Request master- All Request
	#
	#
	#
	#Persona- *Requester*
	#
	#Scenario: Cancel Request by requester for returned status
	#Given that i am on my request listing page
	#And I click on cancel action for a request in returned status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in my request listing
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for request manager
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#
	#Scenario: Cancel Request by requester for “With RM” status
	#Given that i am on my request listing page
	#And I click on cancel action for a request in “With RM” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in my request listing
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for request manager
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#
	#Scenario: Cancel Request by requester for “In progress” status with icontract as end point
	#Given that i am on my request listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking  “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iContract
	#And the request status should be changed to cancelled in my request listing
	#And the contract should be deleted in iContract
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for request manager
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#
	#And I should not be able to view the contract on click of view details on linked document
	#
	#Scenario: Cancel Request by requester for “In progress” status with icontract as end point
	#Given that i am on my request listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from iContract
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#
	#Scenario: Cancel Request by requester for “In progress” status with isource as end point
	#Given that i am on my request listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iSource
	#And the request status should be changed to cancelled in my request listing
	#And the sourcing event should be deleted in iSource
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for request manager
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#And I should not be able to view the sourcing event on click of view details on linked document
	#
	#Scenario: Cancel Request by requester for “In progress” status with isource as end point
	#Given that i am on my request listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from isource
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel request should not be available for request in "In Progress" status for iManage as end point
	#Given that i am on my request listing page
	#Then  I should not get cancel action for a request in “In progress” status with iManage as end point
	#
	#Scenario: Cancel Request by requester for “Awaiting approval” status
	#Given that i am on my request listing page
	#And I click on cancel action for a request in “Awaiting approval” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in my request listing
	#And the audit trail should show the cancel request entry with comments
	#And the request should be removed for the current approvers listing page
	#And the request status should not change for the approver who has already taken action
	#And the request status should be changed to cancelled for request master
	#
	#----
	#
	#Persona- Request Manager
	#
	#Scenario: Cancel Request by Request Manager  for returned status
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in returned status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in my workbench  listing
	#And the audit trail should show the cancel request entry with comments for request manager
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#
	#Scenario: Cancel Request by Request Manager  for “With RM” status
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in “With RM” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in my workbench  listing
	#And the audit trail should show the cancel request entry with comments for request manager
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#
	#Scenario: Cancel Request by Request Manager  for “In progress” status with icontract as end point
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iContract
	#And the request status should be changed to cancelled in my workbench  listing
	#And the contract should be deleted in iContract
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#And I should not be able to view the contract on click of view details on linked document
	#
	#Scenario: Cancel Request by Request Manager  for “In progress” status with icontract as end point
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from iContract
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel Request by Request Manager  for “In progress” status with isource as end point
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iSource
	#And the request status should be changed to cancelled in my workbench  listing
	#And the sourcing event should be deleted in iSource
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super request manager
	#And the request status should be changed to cancelled for request master
	#And I should not be able to view the sourcing event on click of view details on linked document
	#
	#Scenario: Cancel Request by Request Manager  for “In progress” status with isource as end point
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from isource
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel request should not be available for request in "In Progress" status for iManage as end point
	#Given that i am on my workbench  listing page
	#Then I should not get cancel action for a request in “In progress” status with iManage as end point
	#
	#Scenario: Cancel Request by Request Manager  for “Awaiting approval” status
	#Given that i am on my workbench  listing page
	#And I click on cancel action for a request in “Awaiting approval” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in my workbench  listing
	#And the audit trail should show the cancel request entry with comments
	#And the request should be removed for the current approvers listing page
	#And the request status should not change for the approver who has already taken action
	#And the request status should be changed to cancelled for request master
	#
	#----
	#
	#Persona- Super Request Manager
	#
	#Scenario: Cancel Request by Super Request Manager   for returned status
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in returned status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in All workbench listing
	#And the audit trail should show the cancel request entry with comments for Super Request Manager
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Super Request Manager
	#And the request status should be changed to cancelled for request master
	#
	#Scenario: Cancel Request by Super Request Manager   for “With RM” status
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in “With RM” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in All workbench listing
	#And the audit trail should show the cancel request entry with comments for Super Request Manager
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Super Request Manager
	#And the request status should be changed to cancelled for request master
	#
	#Scenario: Cancel Request by Super Request Manager   for “In progress” status with icontract as end point
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iContract
	#And the request status should be changed to cancelled in All workbench listing
	#And the contract should be deleted in iContract
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Super Request Manager
	#And the request status should be changed to cancelled for request master
	#And I should not be able to view the contract on click of view details on linked document
	#
	#Scenario: Cancel Request by Super Request Manager   for “In progress” status with icontract as end point
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from iContract
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel Request by Super Request Manager   for “In progress” status with isource as end point
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iSource
	#And the request status should be changed to cancelled in All workbench listing
	#And the sourcing event should be deleted in iSource
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Super Request Manager
	#And the request status should be changed to cancelled for request master
	#And I should not be able to view the sourcing event on click of view details on linked document
	#
	#Scenario: Cancel Request by Super Request Manager   for “In progress” status with isource as end point
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from isource
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel request should not be available for request in "In Progress" status for iManage as end point
	#Given that i am on All workbench listing page
	#Then I should not get cancel action for a request in “In progress” status with iManage as end point
	#
	#Scenario: Cancel Request by Super Request Manager   for “Awaiting approval” status
	#Given that i am on All workbench listing page
	#And I click on cancel action for a request in “Awaiting approval” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in All workbench listing
	#And the audit trail should show the cancel request entry with comments
	#And the request should be removed for the current approvers listing page
	#And the request status should not change for the approver who has already taken action
	#And the request status should be changed to cancelled for request master
	#
	#----
	#
	#
	#
	#Persona- Request Master
	#
	#Scenario: Cancel Request by Request Master for returned status
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in returned status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in All request  listing
	#And the audit trail should show the cancel request entry with comments for Request Master
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Request Master
	#And the request status should be changed to cancelled for requester
	#
	#Scenario: Cancel Request by Request Master for “With RM” status
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in “With RM” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in All request  listing
	#And the audit trail should show the cancel request entry with comments for Request Master
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Request Master
	#And the request status should be changed to cancelled for requester
	#
	#Scenario: Cancel Request by Request Master for “In progress” status with icontract as end point
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iContract
	#And the request status should be changed to cancelled in All request  listing
	#And the contract should be deleted in iContract
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Request Master
	#And the request status should be changed to cancelled for requester
	#And I should not be able to view the contract on click of view details on linked document
	#
	#Scenario: Cancel Request by Request Master for “In progress” status with icontract as end point
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in “In progress” status with icontract as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from iContract
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel Request by Request Master for “In progress” status with isource as end point
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should be cancelled if it has the required permission from iSource
	#And the request status should be changed to cancelled in All request  listing
	#And the sourcing event should be deleted in iSource
	#And the audit trail should show the cancel request entry with comments
	#And the request status should be changed to cancelled for requester
	#And the request status should be changed to cancelled for super Request Master
	#And the request status should be changed to cancelled for requester
	#And I should not be able to view the sourcing event on click of view details on linked document
	#
	#Scenario: Cancel Request by Request Master for “In progress” status with isource as end point
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in “In progress” status with isource as end point
	#And I enter reason for cancellation
	#When I click on cancel
	#Then there should be a confirmation pop up asking “ “Cancelling the request would discard the document in corresponding product”
	#And I select Yes on the pop up
	#Then the request should NOT be cancelled if it do NOT have the required permission from isource
	#And the user should be displayed an alert message "Unable to cancel the request. Please contact administrator."
	#
	#Scenario: Cancel request should not be available for request in "In Progress" status for iManage as end point
	#Given that i am on All request  listing page
	#Then I should not get cancel action for a request in “In progress” status with iManage as end point
	#
	#Scenario: Cancel Request by Request Master for “Awaiting approval” status
	#Given that i am on All request  listing page
	#And I click on cancel action for a request in “Awaiting approval” status
	#And I enter reason for cancellation
	#When I click on cancel
	#Then the request should be cancelled
	#And the request status should be changed to cancelled in All request  listing
	#And the audit trail should show the cancel request entry with comments
	#And the request should be removed for the current approvers listing page
	#And the request status should not change for the approver who has already taken action
	#And the request status should be changed to cancelled for requester
	#
	#----
	#
	#Access control scenario
	#
	#Scenario: Requester should be able to see cancel request in my request listing page as secondary action if he has ”Access to cancel request” in TMS
	#Given I am on my request listing page
	#And I have activity assigned  ”Access to cancel request”
	#Then I should be able to see cancel request in my request listing
	#
	#Scenario: Request Manager should be able to see cancel request in my workbench listing page as secondary action if he has ”Access to cancel request” in TMS
	#Given I am on my request listing page
	#And I have activity assigned  ”Access to cancel request”
	#Then I should be able to see cancel request in my workbench listing
	#
	#Scenario: Super Request Manager should be able to see cancel request in all workbench listing page as secondary action if he has ”Access to cancel request” in TMS
	#Given I am on my request listing page
	#And I have activity assigned  ”Access to cancel request”
	#Then I should be able to see cancel request in all workbench listing
	#
	#Scenario: Request Master should be able to see cancel request in all workbench listing page as secondary action if he has ”Access to cancel request” in TMS
	#Given I am on my request listing page
	#And I have activity assigned  ”Access to cancel request”
	#Then I should be able to see cancel request in all request listing
	#
	#----
	#
	#Scenario: Transfer request based on cancelled status
	#Given I am on Transfer request ownership page
	#And I select transfer request based on status
	#Then I should be able to do transfer request based on "Cancelled" status
	#And I should be able to see transferred requests based on canceleed status under impacted requests
	#
	#Scenario: Cancel request should not be visible for the request in completed status
	#Given I am on my request listing page
	#And I click on secondary action for a request in completed status
	#Then I should not be able to see cancel action in secondary action
	#And I am on my workbench listing page
	#And I click on secondary action for a request in completed status
	#Then I should not be able to see cancel action in secondary action
	#And I am on all workbench listing page
	#And I click on secondary action for a request in completed status
	#Then I should not be able to see cancel action in secondary action
	#And I am on all request listing page
	#And I click on secondary action for a request in completed status
	#Then I should not be able to see cancel action in secondary action
	#
	#Scenario: CRMS updates on cancelled request
	#Given I mark the request as completed from my request listing page
	#Then the request status should be updated in CRMS as cancelled
	#
	#Scenario: CRMS updates on cancelled request
	#Given I mark the request as completed from my workbench listing page
	#Then the request status should be updated in CRMS as cancelled
	#
	#Scenario: CRMS updates on cancelled request
	#Given I mark the request as completed from all wokbench listing page
	#Then the request status should be updated in CRMS as cancelled
	#
	#Scenario: CRMS updates on cancelled request
	#Given I mark the request as completed from all request listing page
	#Then the request status should be updated in CRMS as cancelled
	#
	#Scenario: Requester can copy a cancelled request
	#Given i am on my request listing page
	#And i click on copy action on a cancelled request
	#Then I should be able to copy the request
	#And I should be able to view all the request detailes in the copied request
	#Then I should be able to submit the cancelled request
	#
	#Scenario: Cancel filter on my request listing page should populate only cancelled requests
	#Given I am on my request listing page
	#And I apply status filter with cancelled request
	#Then I should be able to see the cancelled filtered request in the listing
	#
	#Scenario: Cancel filter on my workbench listing page should populate only cancelled requests
	#Given I am on my request listing page
	#And I apply status filter with cancelled request
	#Then I should be able to see the cancelled filtered request in the listing
	#
	#Scenario: Cancel filter on all workbench listing page should populate only cancelled requests
	#Given I am on my request listing page
	#And I apply status filter with cancelled request
	#Then I should be able to see the cancelled filtered request in the listing
	#
	#Scenario: Cancel filter on all request listing page should populate only cancelled requests
	#Given I am on my request listing page
	#And I apply status filter with cancelled request
	#Then I should be able to see the cancelled filtered request in the listing
	#
	#Scenario: No action (recall, delete) available on cancelled request to requester
	#Given i am on my request listing page
	#And i click on secondary action of a cacelled request
	#Then I should not be able to recall or delete the cancelled request
	#
	#Scenario: No action (complete, return, create activity) available on cancelled request to request manager on listing and detail page
	#Gven I am on my workbench listing
	#And I click on secondary action of a cancelled request
	#Then I should not be able to see complete, return, create activity action on the cancelled request
	#And I go on view request page
	#Then I should not be able to see complete, return, create activity action on the cancelled request
	#
	#Scenario: No action (complete, return, create activity) available on cancelled request to request manager on listing and detail page
	#Gven I am on all workbench listing
	#And I click on secondary action of a cancelled request
	#Then I should not be able to see complete, return, create activity action on the cancelled request
	#And I go on view request page
	#Then I should not be able to see complete, return, create activity action on the cancelled request
	#
	#Scenario: No action (complete, return, create activity) available on cancelled request to request manager on listing and detail page
	#Gven I am on all request listing
	#And I click on secondary action of a cancelled request
	#Then I should not be able to see complete, return, create activity action on the cancelled request
	#And I go on view request page
	#Then I should not be able to see complete, return, create activity action on the cancelled request
	#
	#Scenario: Verify if the cancel comments limit is 255 characters
	#Given I cancel a request
	#And I update the comments for cancellation
	#Then I should be able to update comments upto 255 characters
	#And if the character limit exceeds
	#Then I should get an error message "Maximum character limit: 255"
	#
	#Scenario: User email trigerred on cancel request
	#Given I cancel a request
	#Then email should be triggered to  the user taking the action, requester and the user with whom if the request is pending (approver/ RM)
	#
	#*_Scenario: Mobile to show cancelled request as a new status in my request listing page_*

	@TEST_DDS-186797 @DDS-183186
	Scenario: User should be able to upload files with file name upto 255 characters including extensions on iRequest flexiform attachment field
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for flexi field
		Then I upload a file with file name upto 255 characters on iRequest flexiform attachment field
		Then I should be able to submit the request
	@TEST_DDS-186798 @DDS-183186
	Scenario: User should be able to upload files with file name upto 255 characters including extensions on iRequest standard attachment field
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for standard field
		Then I upload a file with file name upto 255 characters on iRequest standard attachment field
		Then I should be able to submit the request
	@TEST_DDS-186799 @DDS-183186
	Scenario: User should be able to download flexiform attachment files with file name upto 255 characters including extensions
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for flexi field
		Then I upload a file with file name upto 255 characters on iRequest flexiform attachment field
		Then I should be able to submit the request
		Then I am on View request page of flexiform
		Then I should be able download flexi form attachment upto 255 character
	@TEST_DDS-186800 @DDS-183186
	Scenario: User should be able to download files with file name upto 255 characters including extensions
		Given I logged in on iRequest Page
		Then I am on MyRequest Listing
		Given I create a request definition for standard field
		Then I upload a file with file name upto 255 characters on iRequest standard attachment field
		Then I should be able to submit the request
		Then I am on View request page
		Then I should be able download standard attachment upto 255 character

@REQ_DDS-185690
Feature: AVOC Mercury: Executive Summary for Internal Signers

	@TEST_DDS-186664 @REQ_DDS-185557 @tagL1
	Scenario: To verify whether user is able to withdraw as well as sign contract send for DocuSign
		Given logged in to iContract Login Page
		Given I Navigate to iContract Settings Tab followed by Product Configuration
		Then I enable withdraw signing from settings
		Given I navigate to Authoring Contract Tab
		And I Initiate Authoring Contract with Template form Template Library where I set contract Title in "GlobalVariable"
		Then I enter Contract Details
		When I select Contracting Party And Contract Person
		When I save the authoring contract
		Then I Send For Contracting Party under Negotiate Stage
		Then I upload the supplier copy
		Then I wait for "15" seconds
		Then I initiate proceed to signing
		Then I send for online signing to "Docusignmail"
		When I wait for "15" seconds
		When I refresh the page
		Then I withdraw from signing and send back for signing
		Then I navigate to docusign page and sign the document
		Then I open dewdrops home
		Then I navigate to Authoring Contract Tab from Repo
		Then I check Signed status of contract

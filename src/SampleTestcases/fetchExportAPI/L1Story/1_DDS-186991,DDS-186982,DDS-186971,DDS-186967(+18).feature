Feature: 

	@TEST_DDS-186991 @tagL1
	Scenario: Precanned Report in listing page
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I add field and click on save button
		Given I get the report Name
		Then I save the Report
		Given I click on back button on report creation page
		Given I select "MyReportsView" view of "Default_Views" from View dropdown
		Then I search for Report
		Then I select option Report name to search the report
		Given I select Precanned Report in Secondary
		Then I confirm precanned Report
	@TEST_DDS-186982 @tagL1
	Scenario: Deleting Subscription of a report
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I add field and click on save button
		Given I get the report Name
		Then I save the Report
		Given I click on back button on report creation page
		Given I select "MyReportsView" view of "Default_Views" from View dropdown
		Then I search for Report
		Then I select option Report name to search the report
		When I select Subscribe Report in Secondary
		When I  Fill detail for daily report
		Then I select the File Type
		Then I Subscribe Report
		Given User navigates to Mysubscription Page
		Then I search for Report
		Then I select option Report name to search the report
		When I select Delete Subscription Option from Secondary Option
		Then I click on Yes button
		Then I verify Report is not seen
	@TEST_DDS-186971 @tagL0 @tagL1
	Scenario: Inline Filter for Dimension Field
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on create Report
		Then I select field of "EventStage_iSource" of "Product_iSource"
		Then I Apply Fields
		When I click on Inline Filter Icon
		Then I filter Status field
		Then I apply Inline filter
	@TEST_DDS-186967 @tagL1
	Scenario: Edit created CustomField Name
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I click on create Report
		When I am on MyField Page
		When I edit CustomField
		Then I set Feild name
		Then I create Custom Field
	@TEST_DDS-186966 @tagL1
	Scenario: Validation Check while creating Custom Field
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I click on create Report
		When I am on MyField Page
		When I click on Create New CustomField
		Then I set Feild name
		Then I add a Field and A Opertor
		Then I create Custom Field
		Then I verify validation message is seen
	@TEST_DDS-186965 @tagL1
	Scenario: Edit Created Custom Field Formula
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I select "ViewAll_View" view of "My_views" from View dropdown
		Given I click on create Report
		When I am on MyField Page
		When I edit CustomField
		Then I remove the field added in the formula for created CustomField
		Then I create Custom Field
	@TEST_DDS-186963 @tagL1
	Scenario: Verifying BulkUpload option is not seen Menu Options
		Given I logged in on CRMS Page
		Given I click on Menu Option
		Then I verify bulkUpload option is not seen
	@TEST_DDS-186962 @tagL0 @tagL1
	Scenario: Access Reports by report quicklink
		Given I logged in on CRMS Page
		Given I navigate to Report page through report quicklink
	@TEST_DDS-186959 @tagL0 @tagL1
	Scenario: My Desk has report which redirected to report listing page
		Given I logged in on CRMS Page
		Given User navigates to My Desk Page
		When I navigate to Report listing page
	@TEST_DDS-186958 @tagL0 @tagL1
	Scenario: On searches for Insight Studio, able to redirect all links
		Given I logged in on CRMS Page
		Given I search a "Insight Studio" in Home page and click on "Create a New Report" link
		When Verify link redirected to Add fields
		When I click on cross icon
		When User navigates to Home Page
		Given I search a "Insight Studio" in Home page and click on "View Reports" link
		When Verify link redirected to view report page
		When User navigates to Home Page
		Given I search a "Insight Studio" in Home page and click on "Export Reports" link
		When Verify link redirected to Export Tab
		When User navigates to Home Page
		Given I search a "Insight Studio" in Home page and click on "Subscribe" link
		When Verify link redirected to Subscribe Tab
	@TEST_DDS-186955 @tagL1
	Scenario: Create CustomField and delete it after checking report name is correct in delete popup
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I click on create Report
		When I am on MyField Page
		When I click on Create New CustomField
		Then I set Custom Field name
		Then I add the field with Operators into Input Field
		Then I create Custom Field
		Then I select field of "EventStage_iSource" of "Product_iSource"
		Then I search and select for the custom field to be deleted
		Then I Apply Fields
		Then I fill report details on create page
		Then I save the Report
		Then I select Create New Report in Secondary option
		When I am on MyField Page
		Then I search and select for the custom field to be deleted
		Then I click on delete icon of the custom field
		Then I check if report in adhoc reports section is correct
		Then I click on Yes button in CF deletion popup
	@TEST_DDS-186953 @tagL1
	Scenario: Export report containing Custom field
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on create Report
		Then I am on MyField Page
		Then I select Calculated Field with Non Precanned
		Then I Apply Fields
		Then I fill report Details
		Then I save the Report
		Then I export Within Report
	@TEST_DDS-186952 @tagL1
	Scenario: Not able to share report containing non-precanned CF
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on create Report
		Then I am on MyField Page
		Then I select Calculated Field with Non Precanned
		Then I Apply Fields
		Then I fill report Details
		Then I save the Report
		Then I verify Share button Disabled
	@TEST_DDS-186948 @tagL0 @tagL1
	Scenario: Create Report with Calculated Field
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on create Report
		Then I am on MyField Page
		Then I select Calculated Field with Non Precanned
		Then I Apply Fields
		Then I fill report Details
		Then I save the Report
	@TEST_DDS-186947 @tagL1
	Scenario: To verify that when user clicks on save As and saves the report his old report should not be overwritten
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I select "ViewAll_View" view of "My_views" from View dropdown
		Then I fetch the name of first report
		Then I open first report from Listing page
		Then I Click on Save as button and save the report
		Then I verify old report is not overwritten
	@TEST_DDS-186946 @tagL0 @tagL1
	Scenario: Navigate to all tabs
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given User navigates to Offline Export Page
		Given User navigates to Mysubscription Page
	@TEST_DDS-186945 @tagL0 @tagL1
	Scenario: create report Using  Dimension and measures
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on create Report
		Then I select field of Dimension
		Then I select field of Measure
		Then I Apply Fields
		Then I fill report Details
		Then I save the Report
	@TEST_DDS-186944 @tagL0 @tagL1
	Scenario: create cross product report
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on create Report
		Then I select field of Dimension
		Then I select field of Description
		Then I Apply Fields
		Then I fill report Details
		Then I save the Report
	@TEST_DDS-186940 @tagL0 @tagL1
	Scenario: create CustomField with basic formula
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I click on create Report
		When I am on MyField Page
		When I click on Create New CustomField
		Then I set Feild name
		Then I get the field name
		Then I add the field with Operators into Input Field
		Then I mark as Precanned
		Then I create Custom Field
		Then I click close alert panel icon
		Then I verify CF is created
	@TEST_DDS-186937 @tagL0 @tagL1
	Scenario: Share report in Bulk
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I select "ViewAll_View" view of "My_views" from View dropdown
		Given I filter Folder column
		Given I select Multiple report
		When I click on Share Report on Footer
		When I select User for Share
		Then I save Share Report popup
	@TEST_DDS-186936 @tagL0 @tagL1
	Scenario: Export report in Bulk
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I create Report
		Given I click on back to listing page icon
		Given I create Report
		Given I click on back to listing page icon
		Given I select "MyReportView" view of "Default_Views" from View dropdown
		When I select Multiple report
		When I click on Export Report on Footer
		Then I Export Report from footer
	@TEST_DDS-186935 @tagL0 @tagL1
	Scenario: Change Folder in Bulk
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I filter Creator column
		Given I select "MyReportView" view of "Default_Views" from View dropdown
		Given I filter Folder "FolderOption"
		Given I select Multiple report
		When I click on Change Folder on Footer
		Then I Change Folder from Footer

@REQ_DDS-185674
Feature: Prod Issue- Exports handling for concurent exports

	@TEST_DDS-186987 @REQ_DDS-185620 @tagL1
	Scenario: Export the report from listing page and downloading it from Offline Export page
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on that Export button of first report
		Then I should be able to see Export pop up
		When I enter report name
		When I select XLS File Type in the popup
		When I click on Export button
		When I click on Dismiss button
		Given I click on Offline Exports tab on Listing page
		When I click on Download button coresponding to report exported of Format
	@TEST_DDS-186986 @REQ_DDS-185620 @tagL0 @tagL1
	Scenario: Export a report after opening it from listing page
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		Given I select "ViewAll_View" view of "My_views" from View dropdown
		Given I search for particular report
		When I click on that Report
		When I click on Export Icon
		When I enter report name
		When I select XLS File Type in the popup
		When I click on Export button
		When I click on Dismiss button
	@TEST_DDS-186984 @REQ_DDS-185620 @tagL1
	Scenario: Export the report from listing page and downloading it from Export Progress Status dialog box on listing page
		Given I logged in on CRMS Page
		Given User navigates to Report Page
		When I click on that Export button of first report
		Then I should be able to see Export pop up
		When I enter report name
		When I select PDF File Type in the popup
		When I click on Export button
		When I click on Dismiss button
		Given I see the Export progress status dialog box
		When I click on Expand button on dialog box
		When I click on download button of the corresponding report

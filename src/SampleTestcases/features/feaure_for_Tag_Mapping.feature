@REQ_TSTNG-322
Feature: Feature for tag mapping

@TEST_TSTNG-323  @tagL1 @TSTNG-322
    Scenario: scenario one created for tag mapping
        Given loggin to application
        When tagged an story
        Then should see the story in linked issue


@TEST_TSTNG-324  @L1 @TSTNG-322
    Scenario: scenario two created to check feature update is impacting the linkage
        Given loggin to application
        When new scenario added to same files
        Then scenario should be added to the story


@TEST_TSTNG-325  @tagL1 @TSTNG-322
    Scenario: scenario three created to understand the issue with import cucumber test feature
        Given loggin to application
        When understand the issue with import cucumber test feature
        Then scenario should be added to the story


@TEST_TSTNG-326  @tagL1 @TSTNG-322
    Scenario: scenario four created to reiterate the same scenario as scenario three
        Given loggin to application
        When re-iterate the same scenario as scenario three
        Then scenario should be added to the story
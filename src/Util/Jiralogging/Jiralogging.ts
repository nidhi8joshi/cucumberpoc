
const fetch = require("node-fetch");


export class Jiralogger {

  static async logJira(ScenarioName: string, FeatureName: string, errorStack: string, flag: boolean) {
    return new Promise(async function (resolve, reject) {
      if (flag) {
        const bodyData = `{
    "fields": {
       "project":
       {
          "key": "TSTNG"
       },
       "summary": "`+ ScenarioName + ` SCENARIO has failed from FEATURE ` + FeatureName + `",
        "assignee": { "id": "5ca5be5f9a000c1180956a33" },
        "issuetype": {
            "name": "Bug"
        }
   }
} `;

        fetch('https://pdtzycus.atlassian.net/rest/api/2/issue', {
          method: 'POST',
          headers: {
            'Authorization': `Basic SklSQS1ib3R1c2VyQHp5Y3VzLmNvbTpRbnhXTVRKd0pQc0Q4aGpNMFdoVjlENTM=`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: bodyData
        })
          .then(async function (response: any) {
            console.log(
              `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
          })
          .then(async function (text: any) {
            resolve(text)
          })
          .catch(async function (err: any) {
            reject(err)
          });
      }
      else {
        resolve()
      }
    });
  }

}


// "components": [{ "name": "`+ process.env.PRODUCT_COMPONENT + `" }],
//   "assignee": { "name": "`+ process.env.ASSIGNEE + `" },
// "customfield_15709": { "value": "`+ process.env.PRIORITY + `" },
// "customfield_15606": { "value": "`+ process.env.SEVERITY + `" },
// "customfield_16203": { "value": "Yes" }
// "description": "`+ ((errorStack.replace(/\\/g, "\\\\")).replace(/\"/g, "\\\"")).replace(/\n/g, "") + `",
// "customfield_10128": { "value": "`+ process.env.PRODUCT + `" },



// {
//   "fields": {
//     "project":
//     {
//       "key": "TSTNG"
//     },
//     "summary": "Create English Auction Event @tagL1 @JIRA SCENARIO has failed from FEATURE English Auction Event",
//       "components": [{ "name": "DDS iSource" }],
//         "assignee": { "id": "5cc00d0ac2e4550e3df41fbf" },
//     "description": "expected web page to include \"cucumber.io\"Scenario Steps:- I.see(\"cucumber.io\") at D:\\POC_framework\\JiraIntegration\\src\\SampleTestcases\\implementation\\googleimpl.ts:17:7- I.fillField(\"//input[@name='q']\", \"cucumber bdd\") at D:\\POC_framework\\JiraIntegration\\src\\SampleTestcases\\implementation\\googleimpl.ts:12:7- I.amOnPage(\"https://www.google.com/\") at D:\\POC_framework\\JiraIntegration\\src\\SampleTestcases\\implementation\\googleimpl.ts:7:7",
//       "issuetype": {
//       "name": "Bug"
//     },
// "customfield_10128": { "value": "iSource" },
// "customfield_15709": { "value": "P1" },
// "customfield_15606": { "value": "Major" },
// "customfield_16203": { "value": "Yes" }
//   }
// } 

var request = require('request');
var fs = require('fs');

export class xray_ImportCucumberApiRequest {

    static async importCucumberFeatureRequest(projectKey: string) {
        await xray_ImportCucumberApiRequest.generateAuthToken().then(async function (authToken) {

            let finalToken: string = 'Bearer ' + authToken.replace(/(\")/gm, "")
            var options = {
                'method': 'POST',
                'url': 'https://xray.cloud.xpand-it.com/api/v1/import/feature?projectKey=' + projectKey,
                'headers': {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': finalToken
                },
                formData: {
                    'file': {
                        'value': fs.createReadStream('D:/POC_framework/Codecept_iContract/src/iContract/features/Clauses/Clauses.feature'),
                        'options': {
                            'filename': 'D:/POC_framework/Codecept_iContract/src/iContract/features/Clauses/Clauses.feature',
                            'contentType': null
                        }
                    }
                }
            };
            console.log(options)
            request(options, function (error: any, response: any) {
                if (error) throw new Error(error);
                console.log(JSON.parse(response.body));
            });
        })
    }

    static async generateAuthToken() {
        let token: string;
        return new Promise<string>((resolve, reject) => {
            var options = {
                'method': 'POST',
                'url': 'https://xray.cloud.xpand-it.com/api/v1/authenticate',
                'headers': {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ "client_id": "79279168D5CB4D7284FA990D13227F8A", "client_secret": "b45f7efeb7639fcc1a38c88e7040e22fcfae4c14f9a4b02123a500803dc7aaac" })

            };
            request(options, async function (error: any, response: any) {
                if (error)
                    reject(error);
                else {
                    token = response.body
                    resolve(token);
                }

            });
        })


    }
}

// module.exports = new xray_ImportCucumberApiRequest();
// xray_ImportCucumberApiRequest.importCucumberFeatureRequest("TSTNG");


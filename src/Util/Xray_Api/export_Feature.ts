import { xray_ImportCucumberApiRequest } from "./xray_ImportFeature_Request";
import console from "console";
import { Startup } from "../../Framework/FrameworkUtilities/Startup/Startup";
var request = require('request');
var fs = require('fs');
let filter_output: any;
export class xray_ExportCucumberApiRequest {

    static async exportCucumberFeatureRequest(fetchParam: string) {

        await xray_ImportCucumberApiRequest.generateAuthToken().then(async function (authToken) {

            let finalToken: string = 'Bearer ' + authToken.replace(/(\")/gm, "")
            var options = {
                'method': 'GET',
                'url': 'https://xray.cloud.xpand-it.com/api/v1/export/cucumber?filter=50819',
                'headers': {
                    'Content-Type': 'application/json',
                    'Authorization': finalToken
                }
            };
            request(options, function (error: any, response: any) {
                if (error) throw new Error(error);
                console.log(response.body)

            });

        })
    }

    static async fetchFilterResult() {
        return new Promise(async (resolve: any, reject: any) => {
            var options = {
                'method': 'GET',
                'url': 'https://pdtzycus.atlassian.net/rest/api/3/search?jql=filter=50833',
                'headers': {
                    'Accept': 'application/json',
                    'Authorization': 'Basic SklSQS1ib3R1c2VyQHp5Y3VzLmNvbTpRbnhXTVRKd0pQc0Q4aGpNMFdoVjlENTM='
                }
            };
            request(options, async function (error: any, response: any) {
                if (error) throw new Error(error);
                resolve(JSON.parse(response.body))
            });
        })
    }

    static async process_ScenarioLabel() {
        await xray_ExportCucumberApiRequest.fetchFilterResult().then(async function (json: any) {
            let issues_Data: JSON[] = json["issues"]
            for (let i = 0; i < issues_Data.length; i++) {
                Startup.finalScenarioLabel_map.set((issues_Data[i]["fields"])["summary"], issues_Data[i]["key"])
            }
            console.log(Startup.finalScenarioLabel_map)
        })
    }
}

// xray_ExportCucumberApiRequest.process_ScenarioLabel()


// get scenario name from the feature file
// (?<= Scenario: )[a - z A - Z \d]*



const recorder = require('codeceptjs').recorder;
const request = require('request');
import { event } from 'codeceptjs';
import { Jiralogger } from "../../Util/Jiralogging/Jiralogging";
import { xray_ImportCucumberApiRequest } from '../Xray_Api/xray_ImportFeature_Request';
const jsesc = require('jsesc');
const moment = require('moment');
let errorOccured = false;
var JiraloggingFlag = (process.env.JiraloggingFlag == "true");

var info = "";
var tests = "";
let json: any;
let promises: any = [];
let promisesfirst: any = []
const config = {
  debug: false,
  jira_url: 'https://localhost:8080',
  jira_user: 'root',
  jira_password: 'root',
  test_revison: '001',
  testEnvironments: '["browser:chrome", "linux"]'
};
export class eventListener {
  static async init() {

    event.dispatcher.on(event.test.started, async function () {
      console.log('--- I am before test --');

    })

    event.dispatcher.on(event.test.after, async function (test) {
      console.log('--- I am after test --', test.tags[0]);
      let status: any;
      let comment: any;
      let defect: any;
      if (test.state == "passed") {
        status = 'PASSED';
        comment = "Successful execution";

      } else {
        status = 'FAILED';
        promises.push(new Promise(async (resolve, reject) => {
          await Jiralogger.logJira(test.title, test.parent.title, test.err.stack, JiraloggingFlag).then(async function (json: any) {
            comment = jsesc(test.err.toString().replace(/\"/g, "").replace(/\'/g, "").replace(/\é/g, "e").replace(/\è/g, "e").replace(/\ê/g, "e").replace(/\à/g, "a").replace(/\ù/g, "u").replace(/\x1B/g, "").replace(/\[\d*[m]/g, ""));
            console.log(comment);
            if (json) {
              defect = '["' + json["key"] + '"]'
            }
            else {
              defect = '[]'
            }
            if (tests.length < 1) {
              info = '{ "info" : { "summary" :"Sample summary", "startDate" : "' + moment().format() + '", "finishDate" :"' + moment().format() + '","revision": "' + config.test_revison + '","description" : "Results of test execution " ' + '},';
              tests = '"tests" : [';
              tests = tests + "" + '{"testKey":"' + (test.tags[1].split("@")[1]).split("_")[1] + '","status":"' + status + '","comment" : "' + comment + '","defects" : ' + defect + ' }';
              resolve(tests)
            } else {
              tests = tests + "" + ',{"testKey":"' + (test.tags[1].split("@")[1]).split("_")[1] + '","status":"' + status + '","comment" : "' + comment + '","defects" : ' + defect + ' }';
              resolve(tests)
            }
          })
        }))
      }
    })

    event.dispatcher.on(event.test.failed, async function (test, e) {
      errorOccured = true;
      console.log('--- I am failed test --' + errorOccured);
    })

    event.dispatcher.on(event.test.finished, async function (test, err) {
      console.log('--- I am finished test --' + errorOccured + "  ==  " + JiraloggingFlag);
      // promisesfirst.push(new Promise(async (resolve, reject) => {
      //   if (errorOccured && JiraloggingFlag) {
      //     json = await Jiralogger.logJira(test.title, test.parent.title, test.err.stack)
      //     console.log("Jira bug ID ", json)
      //     resolve(json)
      //   }
      // }))
    })

    event.dispatcher.on(event.all.after, function (suite) {
      console.log('--- I am all after --- ')
      Promise.all(promises).then(async (testcases: any) => {
        console.log("SEND TO XRAY=>" + info + tests + "]}");
        if (config.debug) console.log("SEND TO XRAY=>" + info + tests + "]}");
        recorder.add('Sending new result to xray', function () {
          return new Promise(async (doneFn, errFn) => {
            await xray_ImportCucumberApiRequest.generateAuthToken().then(async function (authToken) {
              let finalToken: string = 'Bearer ' + authToken.replace(/(\")/gm, "")

              var options = {
                'method': 'POST',
                'url': 'https://xray.cloud.xpand-it.com/api/v1/import/execution',
                'headers': {
                  'Content-Type': 'application/json',
                  'Authorization': finalToken
                },
                body: info + tests + "]}"

              };
              request(options, function (error: any, response: any) {
                if (error) doneFn(error);
                else {
                  console.log(response.body)
                  doneFn(response.body);
                }

              });
            });
          });

        });
      })
    })
    event.dispatcher.on(event.suite.after, (suite) => {
      console.log("----- After suite-------")
    });
  }
}
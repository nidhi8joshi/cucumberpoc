
const fs = require('fs')
import { xray_ExportCucumberApiRequest } from "../Util/Xray_Api/export_Feature";
import { Startup } from "../Framework/FrameworkUtilities/Startup/Startup";
var recursive = require("recursive-readdir");
export class fileSystemOperation {

    static async mapTagsToFeature() {
        console.log("Sample added")
        await xray_ExportCucumberApiRequest.process_ScenarioLabel();
        recursive("./src/SampleTestcases/features/", function (err: any, files: any) {
            let arrayOfFeatureFiles = files;
            for (let index = 0; index < arrayOfFeatureFiles.length; index++) {
                let filepath: any = arrayOfFeatureFiles[index];
                fs.readFile(filepath, "utf-8", (err: any, data: any) => {
                    var regexForScenario = new RegExp("(Scenario: [A-Z a-z\\d]*)", "gm");
                    let scenarioList: any = data.match(regexForScenario)
                    console.log("SCENARIO LIST FOR THIS FILE   ", scenarioList)
                    for (let i = 0; i < scenarioList.length; i++) {
                        var scenarioName = scenarioList[i].replace(/Scenario: /gm, '')
                        if (Startup.finalScenarioLabel_map.has(scenarioName)) {
                            console.log(scenarioName, '       @TEST_' + Startup.finalScenarioLabel_map.get(scenarioName))
                            var pattern = '(.*((@[a-z A-Z \\d]*[- : _ ]*[\\d]*))[\\r \\n \\t .]*)(Scenario: ' + scenarioName + ')';
                            var regex = new RegExp(pattern, "m");
                            let matches: any = data.search(regex)
                            let tagToBeAdded = '@TEST_' + Startup.finalScenarioLabel_map.get(scenarioName) + ' '
                            if (data.match(tagToBeAdded)) {
                                console.log("Tag Already added")
                            }
                            else {
                                data = [data.slice(0, matches), tagToBeAdded, data.slice(matches)].join('');
                                console.log("Tag is newly added to scenario")
                            }
                        }
                    }
                    fs.writeFile(filepath, data, (err: any) => {
                        console.log(data)
                        if (err) console.log(err);
                        console.log("Successfully Written to File." + filepath);
                    });
                });
            }
        });
    }
}

fileSystemOperation.mapTagsToFeature();

// D:/POC_framework/JiraIntegration/src/SampleTestcases/features/feaure_for_Tag_Mapping.feature
// D:/POC_framework/JiraIntegration/src/SampleTestcases/features/googleFeature.feature
// fs.readFile("D:/POC_framework/JiraIntegration/src/SampleTestcases/features/feaure_for_Tag_Mapping_SecondFile.feature", "utf-8", (err: any, data: string) => {

//     var d = (data.replace(/\r\n/gm, ""))
//     let match: any = d.match(/((@[a-z A-Z \d :-]*)*)(?<=Scenario: first scenario from second file)/m)
//     if (match) {
//         console.log(match[0])
//         let y = "qwerty " + match[0]
//         let w = d.replace(match[0], y)
//         let x = w.replace(/(\s\s+)/gm, "\r\n")

//         fs.writeFile("D:/POC_framework/JiraIntegration/src/SampleTestcases/features/feaure_for_Tag_Mapping_SecondFilecopy.feature", x, (err: any) => {
//             if (err) console.log(err);
//             console.log("Successfully Written to File.");
//         });
//     }
// });

// if (matches) {
            //     console.log(matches[0])
            //     let updatedTags = "qwerty " + matches[0]
            //     let FileContent = newData.replace(matches[0], updatedTags)
            //     let finalContent = FileContent.replace(/(\s\s+)/gm, "\r\n").replace("Feature", "\nFeature")
            // }

                 // var scenarioName = 'search work on google'
            // var pattern = '(.*((@[a-z A-Z \\d]*[- : _ ]*[\\d]*))[\\r \\n \\t .]*)(Scenario: ' + scenarioName + ')';
            // var regex = new RegExp(pattern, "m");
            // var newData = data
            // console.log(newData)
            // let matches: any = newData.search(regex)
            // console.log(newData.length)
            // console.log(matches)
            // var output = [newData.slice(0, matches), "@qwerty", newData.slice(matches)].join('');
            // console.log(output)

            // fs.writeFile("D:/POC_framework/JiraIntegration/src/SampleTestcases/features/feaure_for_Tag_Mapping_SecondFilecopy.feature", output, (err: any) => {
            //     if (err) console.log(err);
            //     console.log("Successfully Written to File.");
            // });
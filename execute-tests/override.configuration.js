const argv = require('minimist')(process.argv.slice(2))
const fs = require('fs')
const path = require('path')
const {SELENOID_HOST, SELENOID_PORT} = process.env;

function configTemp(config) {
  return `process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
  require("source-map-support").install(); 
  require("./dist/Framework/PropertiesConfigurator/PropertiesConfigurator").parseJson();
   const prop = global.confi_prop;

   exports.config = ${JSON.stringify(config)}
  `
}

function getRandomString(length) {
  const chars = '1234567890asdfghjklqwertyuiopzxcvbnmQWERTYUIOPASDFGHJKZXCVBNM';
  return Array.from(new Array(length)).reduce((acc) => {
    acc += chars[Math.ceil(Math.random() * chars.length)]
    return acc
  }, '')
}

function getOverridedConfigPath(file) {
  const {config} = argv.c
    ? require(path.resolve(process.cwd(), argv.c))
    : require(path.resolve(process.cwd(), './codecept.conf.js'))
  // gherkin and multiple props should be removed from config
  const {helpers, gherkin, multiple, ...restConfig} = config

  if(SELENOID_HOST, SELENOID_PORT) {
    helpers.WebDriver.host = SELENOID_HOST
    helpers.WebDriver.port = +SELENOID_PORT
  }

  gherkin.features = file
  // ovveridded run config
  const newConfig = {gherkin, helpers, ...restConfig};
  const tempConfigPath = path.resolve(process.cwd(), `./${getRandomString(25)}_temp.execution.config.js`)
  // create temp overrided config to execute-tests dir
  fs.writeFileSync(tempConfigPath, configTemp(newConfig))
  // override gherkin object from config path
  return tempConfigPath
}

module.exports = {
  getOverridedConfigPath
}
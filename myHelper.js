const mysql = require('mysql');
const { I } = inject();
const parser = require('mssql-connection-string');
const { Helper } = codeceptjs;
const prop = require("./dist/Framework/FrameworkUtilities/config").prop;
const logger = require("./dist/Framework/FrameworkUtilities/Logger/logger").logger;
class MyHelper extends Helper {

  // before/after hooks
  /**
   * @protected
   */
  _before() {
    // remove if not used
  }

  /**
   * @protected
   */
  _after() {
    // remove if not used
  }

  async getCookie(){
    const browser = this.helpers['WebDriver'].browser;
    let SAAS_TOKEN ;
    return new Promise(async function(resolve, reject){
     (await browser.getCookies()).forEach(async function(a)  {
       if(a.name == "SAAS_COMMON_BASE_TOKEN_ID"){
        SAAS_TOKEN = a.value;
        resolve (SAAS_TOKEN);
       }
      });
    })
    
  }
  // add custom methods here
  // If you need to access other helpers
  // use: this.helpers['helperName']
  async clickIfVisible(selector, ...options) {
    const helper = this.helpers['WebDriver'];
    try {
      const numVisible = await helper.grabNumberOfVisibleElements(selector);

      if (numVisible) {
        return helper.click(selector, ...options);
      }
    } catch (err) {
      console.log('Skipping operation as element is not visible');
    }
  }
  getElement(elementKey) {
    const I = this;
    const element = global.uiElements.get(elementKey);
    return element;
  }
  getData(key) {
    const I = this;
    var value;
    var index = 0;
    if (key.includes("[") && key.includes("]")) {
      let startindex = key.indexOf("[");
      let endtindex = key.indexOf("]");
      index = key.substring(startindex + 1, endtindex);
      key = key.substring(0, startindex);
    }
    else {
      logger.info("warning : no index in key so it will return 0 index value");
    }

    let mapValue = global.testData.get(key);
    if (typeof mapValue == 'undefined') {
      logger.info("Error : Getting null from testData for given field : " + key);
    }
    else {
      let arrayVal = mapValue.split("||");
      if (index < arrayVal.length) {
        value = arrayVal[index];
      }
      else {
        logger.info("warning : index is greater then size returning 0 index value");
        value = arrayVal[0];
      }
    }
    return value;
  }
  async waitForLoadingSymbolNotDisplayed()
  {
    const helper = this.helpers['WebDriver'];
      await helper.waitForInvisible("//div[@class='spinner']", 60);
      // logger.info("Waited for Loading Symbol to go off");
  }
  async checkIfVisible(selector, ...options) {
    const helper = this.helpers['WebDriver'];
    try {
       if( await helper.grabNumberOfVisibleElements(selector)){
    return true;
       };
        

    } catch (err) {
      console.log(err);
    }
  }

  // async clickIfVisible(selector, ...options) {
  //   const I = this;
  //   const helper = this.helpers['WebDriver'];   // move to helper from steps
  //   try {
  //     const numVisible = await helper.grabNumberOfVisibleElements(selector);

  //     if (numVisible) {
  //       return helper.click(selector, ...options);
  //     }
  //   } catch (err) {
  //     console.log('Skipping operation as element is not visible');
  //   }
  // }

  // async checkIfVisible(selector, ...options) {
  //   const I = this;
  //   const helper = this.helpers['WebDriver']; //move to helper file from steps
  //   try {
  //      if( await helper.grabNumberOfVisibleElements(selector)){
  //   return true;
  //      };
        

  //   } catch (err) {
  //     console.log(err);
  //   }
  // }

  async insertIntoDB(key,value){
      
    // const prop = global.confi_prop;

    const connectionString = "Data Source=tcp:"+prop.DBhost+",3306;Initial Catalog="+prop.DBdatabase+";User Id="+prop.DBuser+";Password="+prop.DBpassword+";";
    logger.info("connectionString  : " + connectionString)

    const connectionObj = parser(connectionString);

    const columnName = process.env.SETUP + "_" + process.env.TENANT
    logger.info(columnName);
    
   
    const query = `update ${prop.testdataTable} set ${columnName}='${value}' where FIELD_NAME='${key}'`
    const connection = mysql.createConnection(connectionObj);

    logger.info(query);
    connection.query(query, function (error) {

      if(!!error){
        logger.info("Error in the query");
        logger.info(error)
        connection.destroy();
      }else{
        logger.info("Success");
        connection.destroy();
      }
    });
}
async stopBrowser() {

  try {
    console.log("========================>Stop the Browser");
    await this.helpers['WebDriver']._stopBrowser();
  } catch (err) {
    console.log("========================>ERROR"+err);
    console.log("========================>Stopping the Browser Failed...");
  }
} 

async startBrowser() {

    try {
      console.log("========================>Stop the Browser");
      await this.helpers['WebDriver']._startBrowser();
    } catch (err) {
      console.log("========================>ERROR"+err);
      console.log("========================>Stopping the Browser Failed...");
    }
}
async waitForLoadingSymbolNotDisplayed()
    {
        await I.waitForInvisible("//div[@class='spinner']", 60);
        // logger.info("Waited for Loading Symbol to go off");
    }

}

module.exports = MyHelper;
